#!/bin/bash

set -eu

export NODE_ENV=production
export REDIS_PASS="${REDIS_PASSWORD}"

exec /usr/local/bin/gosu cloudron:cloudron ethercalc
