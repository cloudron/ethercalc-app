#!/usr/bin/env node
/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

var execSync = require('child_process').execSync,
    ejs = require('ejs'),
    expect = require('expect.js'),
    fs = require('fs'),
    mkdirp = require('mkdirp'),
    path = require('path'),
    rimraf = require('rimraf'),
    superagent = require('superagent'),
    webdriver = require('selenium-webdriver'),
    url = require('url');

var by = webdriver.By,
    Keys = webdriver.Key,
    until = webdriver.until;

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

describe('Application life cycle test', function () {
    this.timeout(0);
    var chrome = require('selenium-webdriver/chrome');
    var server, browser = new chrome.Driver();
    var app;

    var LOCATION = 'test';
    var TEST_TIMEOUT = 30000;
    var SPREADSHEET_NAME = 'mysheet';
    var TEST_CONTENT = 'Hello There!';

    before(function (done) {
        var seleniumJar= require('selenium-server-standalone-jar');
        var SeleniumServer = require('selenium-webdriver/remote').SeleniumServer;
        server = new SeleniumServer(seleniumJar.path, { port: 4444 });
        server.start();

        done();
    });

    after(function (done) {
        browser.quit();
        server.stop();
        done();
    });

    function waitForElement(elem) {
        browser.wait(until.elementLocated(elem), TEST_TIMEOUT).then(function () {
            return browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
        });
    }

    function getSpreadsheet(callback) {
        browser.get('https://' + app.fqdn + '/' + SPREADSHEET_NAME).then(function () {
            return waitForElement(by.xpath('//p[text()="' + TEST_CONTENT + '"]'));
        }).then(function () {
            callback();
        });
    }

    function addSpreadSheet(done) {
        browser.get('https://' + app.fqdn + '/' + SPREADSHEET_NAME).then(function () {
            return waitForElement(by.id('SocialCalc-edittab'));
        }).then(function () {
            // give some time to sync
            setTimeout(done, 2000);
        });
    }

    function changeSpreadSheet(done) {
        browser.get('https://' + app.fqdn + '/' + SPREADSHEET_NAME).then(function () {
            return waitForElement(by.id('SocialCalc-multilineinput'));
        }).then(function () {
            return browser.findElement(by.id('SocialCalc-multilineinput')).click();
        }).then(function () {
            return waitForElement(by.id('SocialCalc-multilinetextarea'));
        }).then(function () {
            return browser.findElement(by.id('SocialCalc-multilinetextarea')).sendKeys(TEST_CONTENT);
        }).then(function () {
            return browser.findElement(by.xpath('//input[@value="Set Cell Contents"]')).click();
        }).then(function () {
            return waitForElement(by.xpath('//p[text()="' + TEST_CONTENT + '"]'));
        }).then(function () {
            done();
        });
    }

    xit('build app', function () {
        execSync('cloudron build', { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('install app', function () {
        execSync('cloudron install --new --wait --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can get app information', function () {
        var inspect = JSON.parse(execSync('cloudron inspect'));

        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];

        expect(app).to.be.an('object');
    });

    it('can get the main page', function (done) {
        superagent.get('https://' + app.fqdn).end(function (error, result) {
            expect(error).to.be(null);
            expect(result.status).to.eql(200);

            done();
        });
    });

    it('can add a spreadsheet', addSpreadSheet);

    it('can change spreadsheet', changeSpreadSheet);

    it('can access spreadsheet', getSpreadsheet);

    it('can restart app', function () {
        execSync('cloudron restart --wait --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can access spreadsheet', getSpreadsheet);

    it('backup app', function () {
        execSync('cloudron backup create --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('restore app', function () {
        execSync('cloudron restore --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can access spreadsheet', getSpreadsheet);

    it('move to different location', function () {
        execSync('cloudron configure --location ' + LOCATION + '2 --wait --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    });

    it('can access spreadsheet', getSpreadsheet);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    // test update
    it('can install app', function () {
        execSync('cloudron install --new --wait --appstore-id net.ethercalc.cloudronapp --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];
        expect(app).to.be.an('object');
    });
    it('can add a spreadsheet', addSpreadSheet);
    it('can change spreadsheet', changeSpreadSheet);
    it('can update', function () {
        execSync('cloudron install --wait --app ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });
    it('can access spreadsheet', getSpreadsheet);
    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });
});
