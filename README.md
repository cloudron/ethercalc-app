# EtherCalc App

EtherCalc is a web spreadsheet.

## Installation

[![Install](https://cloudron.io/img/button.svg)](https://cloudron.io/button.html?app=net.ethercalc.cloudronapp)

or using the [Cloudron command line tooling](https://cloudron.io/references/cli.html)

```
cloudron install --appstore-id net.ethercalc.cloudronapp
```

## Building

The app package can be built using the [Cloudron command line tooling](https://cloudron.io/references/cli.html).

```
cd ethercalc-app

cloudron build
cloudron install
```
