This app packages EtherCalc 0.20170704.0

### EtherCalc is a web spreadsheet.

Your data is saved on your server, and people can edit the same document
at the same time.

Everybody's changes are instantly reflected on all screens.

Work together on inventories, survey forms, list management,
brainstorming sessions and more!

This app has no concept of users or authentication.
