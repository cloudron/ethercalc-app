FROM cloudron/base:0.10.0
MAINTAINER Johannes Zellner <johannes@cloudron.io>

RUN mkdir -p /app/code
WORKDIR /app/code

ENV PATH /usr/local/node-6.9.5/bin:$PATH

RUN npm install -g ethercalc@0.20170704.0

ADD start.sh /app/code/start.sh

CMD [ "/app/code/start.sh" ]
